#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 20 13:08:15 2019

@author: ramkeerthyathinarayanan
"""

from numpy.random import seed
seed(101)
import tensorflow
tensorflow.random.set_seed(101)

import pandas as pd
import numpy as np

from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout, Conv2D, MaxPooling2D, Flatten
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.metrics import categorical_crossentropy
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.models import Model
from tensorflow.keras.callbacks import EarlyStopping, ReduceLROnPlateau, ModelCheckpoint

import os
import cv2

import imageio
import skimage
import skimage.io
import skimage.transform

from sklearn.utils import shuffle
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import train_test_split
import itertools
import shutil
import matplotlib.pyplot as plt

SAMPLE_SIZE = 78786

IMAGE_SIZE = 50

os.listdir('./IDC_regular_ps50_idx5')

patients = os.listdir('./IDC_regular_ps50_idx5')
len(patients)

all_images_dir = 'all_images_dir'
os.mkdir(all_images_dir)

#!ls

patient_list = os.listdir('./IDC_regular_ps50_idx5')

for patient in patient_list:
    
    path_0 = './IDC_regular_ps50_idx5/' + str(patient) + '/0'
    path_1 = './IDC_regular_ps50_idx5/' + str(patient) + '/1'


    file_list_0 = os.listdir(path_0)
    
    file_list_1 = os.listdir(path_1)

    for fname in file_list_0:

        src = os.path.join(path_0, fname)
        
        dst = os.path.join(all_images_dir, fname)
        
        shutil.copyfile(src, dst)


    for fname in file_list_1:

        src = os.path.join(path_1, fname)
        
        dst = os.path.join(all_images_dir, fname)
        
        shutil.copyfile(src, dst)

len(os.listdir('all_images_dir'))

image_list = os.listdir('all_images_dir')

df_data = pd.DataFrame(image_list, columns=['image_id'])

df_data.head()

def extract_patient_id(x):
    a = x.split('_')
    patient_id = a[0]
    
    return patient_id

def extract_target(x):

    a = x.split('_')
    
    b = a[4]
    
    target = b[5]
    
    return target

df_data['patient_id'] = df_data['image_id'].apply(extract_patient_id)

df_data['target'] = df_data['image_id'].apply(extract_target)

df_data.head(10)

df_data.shape

def draw_category_images(col_name, figure_cols, df, IMAGE_PATH):

    categories = (df.groupby([col_name])[col_name].nunique()).index
    f, ax = plt.subplots(nrows=len(categories),ncols=figure_cols, 
                         figsize=(4*figure_cols,4*len(categories)))
    
    for i, cat in enumerate(categories):
        sample = df[df[col_name]==cat].sample(figure_cols) # figure_cols is also the sample size
        for j in range(0,figure_cols):
            file=IMAGE_PATH + sample.iloc[j]['image_id']
            im=cv2.imread(file)
            ax[i, j].imshow(im, resample=True, cmap='gray')
            ax[i, j].set_title(cat, fontsize=16)  
    plt.tight_layout()
    plt.show()
    
# Draw category
IMAGE_PATH = 'all_images_dir/'

draw_category_images('target', 2, df_data, IMAGE_PATH)

df_data['target'].value_counts()

df_0 = df_data[df_data['target'] == '0'].sample(SAMPLE_SIZE, random_state=101)

df_1 = df_data[df_data['target'] == '1'].sample(SAMPLE_SIZE, random_state=101)

df_data = pd.concat([df_0, df_1], axis=0).reset_index(drop=True)

df_data['target'].value_counts()

y = df_data['target']

df_train, df_val = train_test_split(df_data, test_size=0.10, random_state=101, stratify=y)

print(df_train.shape)
print(df_val.shape)

df_train['target'].value_counts()

df_val['target'].value_counts()

base_dir = 'base_dir'
os.mkdir(base_dir)

train_dir = os.path.join(base_dir, 'train_dir')
os.mkdir(train_dir)

val_dir = os.path.join(base_dir, 'val_dir')
os.mkdir(val_dir)

a_no_idc = os.path.join(train_dir, 'a_no_idc')
os.mkdir(a_no_idc)
b_has_idc = os.path.join(train_dir, 'b_has_idc')
os.mkdir(b_has_idc)

a_no_idc = os.path.join(val_dir, 'a_no_idc')
os.mkdir(a_no_idc)
b_has_idc = os.path.join(val_dir, 'b_has_idc')
os.mkdir(b_has_idc)

os.listdir('base_dir/train_dir')

df_data.set_index('image_id', inplace=True)

train_list = list(df_train['image_id'])
val_list = list(df_val['image_id'])

for image in train_list:
    
    # the id in the csv file does not have the .tif extension therefore we add it here
    fname = image
    
    target = df_data.loc[image,'target']
    
    
    if target == '0':
        label = 'a_no_idc'
    if target == '1':
        label = 'b_has_idc'
    
    src = os.path.join(all_images_dir, fname)
    
    dst = os.path.join(train_dir, label, fname)
    
    shutil.move(src, dst)

for image in val_list:
    
    fname = image
  
    target = df_data.loc[image,'target']
    
    if target == '0':
        label = 'a_no_idc'
    if target == '1':
        label = 'b_has_idc'
    
    src = os.path.join(all_images_dir, fname)
    
    dst = os.path.join(val_dir, label, fname)
    
    shutil.move(src, dst)


# check how many train images we have in each folder

print(len(os.listdir('base_dir/train_dir/a_no_idc')))
print(len(os.listdir('base_dir/train_dir/b_has_idc')))

# check how many val images we have in each folder

print(len(os.listdir('base_dir/val_dir/a_no_idc')))
print(len(os.listdir('base_dir/val_dir/b_has_idc')))
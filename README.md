# Detection of Cancer Tumour from Pathology Image Using Convolutional Neural Network
The main idea of this project is to detect if a particular image contains cancer tumour cell or not using CNN.

### Note: Install tensorflow, keras using pip.

1. Download the dataset from https://www.kaggle.com/paultimothymooney/breast-histopathology-images/download
2. After downloading the dataset execute bc_histopathology_preprocessing.py to preprocess the image files and store it in required directory structure for constructing CNN.
3. It may take about 15 to 30 minutes to preprocess the image sets.
4. After preprocessing is done open jupyter notebook and open BC_CNN_MODEL1.ipynb from jupyter notebook.
5. Execute the python code line by line. During fitting of CNN classifier it may take several hours.

I have also stored the fitted model in the file cnn_bc_model1_final. We can import this fitted model directly to predict classes of image.

bc_data.csv have list of images which are stored while preprocessing it.

bc_prediction_model1.csv have list of predicted values over test set